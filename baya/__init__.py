# Blender COD Tools. Developed by Peter Gubin.

bl_info = {
    "name": "Baya",
    "description": "Blender-Maya bridge.",
    "author": "Peter Gubin",
    "version": (1, 1, 0),
    "blender": (2, 83, 0),
    "location": "Scene settings and popup menu.",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "3D View"}

Modules = ['utils.data_types', 'utils.generic_functions', 'ui.panels.panel_material_manager', 'operators.operators_materials', 'operators.operators_maya_scene_generator', 'operators.operators_info']

modulesNames = Modules

import sys
import importlib

if 'DEBUG_MODE' in sys.argv:
    from operators import *
    from ui.panels import *
    from utils import *
else:
    from .operators import *
    from .ui.panels import *
    from .utils import *


modulesFullNames = {}
for currentModuleName in modulesNames:
    if 'DEBUG_MODE' in sys.argv:
        modulesFullNames[currentModuleName] = ('{}'.format(currentModuleName))
    else:
        modulesFullNames[currentModuleName] = ('{}.{}'.format(__name__, currentModuleName))

for currentModuleFullName in modulesFullNames.values():
    if currentModuleFullName in sys.modules:
        importlib.reload(sys.modules[currentModuleFullName])
    else:
        globals()[currentModuleFullName] = importlib.import_module(currentModuleFullName)
        setattr(globals()[currentModuleFullName], 'modulesNames', modulesFullNames)

def register():
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'register'):
                sys.modules[currentModuleName].register()

def unregister():
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'unregister'):
                sys.modules[currentModuleName].unregister()

if __name__ == "__main__":
    register()
