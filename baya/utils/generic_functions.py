import os, stat, sys
import re
import bpy
from enum import Enum

import threading
import time

debug = False

class NodeType(Enum):
    Texture             = 1
    Principled          = 2
    UV                  = 3
    Invert              = 4
    ShaderMix           = 5
    Normal              = 6
    Mapping             = 7
    MixRGB              = 8
    CombineRGB          = 9
    SeparateRGB         = 10
    ColorRamp           = 11
    Color               = 12
    Value               = 13
    Output              = 14

class ImageDummy(Enum):
    White               = 1
    Gray                = 2
    Black               = 3
    DefaultSpec         = 4
    IdentityNormalMap   = 5

class ColorBlendMode(Enum):
    Normal              = 1
    Disable             = 2
    Multiply            = 3
    SoftLight           = 4

class NormalBlendMode(Enum):
    Normal              = 1
    Add                 = 2
    Disable             = 3

class TextureSet:
    name = ""
    albedo_map = None
    normal_map = None
    specular_map = None
    tint_map = None
    occlusion_map = None
    gloss_map = None
    opacity_map = None
    emissive_map = None


    def print(self):
        if self.albedo_map:
            print("albedo_map: " + self.albedo_map)
        if self.normal_map:
            print("normal_map: " + self.normal_map)
        if self.specular_map:
            print("specular_map: " + self.specular_map)
        if self.tint_map:
            print("tint_map: " + self.tint_map)
        if self.occlusion_map:
            print("occlusion_map: " + self.occlusion_map)
        if self.gloss_map:
            print("gloss_map: " + self.gloss_map)
        if self.opacity_map:
            print("opacity_map: " + self.opacity_map)




def Percentage(part, whole):
  return 100 * float(part)/float(whole)

def CheckForUpdates() -> bool:
    return False

def FindCollection(collection_name: str):
    if bpy.data.collections.get(collection_name) is None:
        return bpy.data.collections.new(collection_name)
    return bpy.data.collections.get(collection_name)

def RecursiveSearchLayerCollection(layer_collection, collection_name):
    found = None
    if (layer_collection.name == collection_name):
        return layer_collection
    for layer in layer_collection.children:
        found = RecursiveSearchLayerCollection(layer, collection_name)
        if found:
            return found

def GetParentCollections(collection, parent_names):
  for parent_collection in bpy.data.collections:
    if collection.name in parent_collection.children.keys():
      parent_names.append(parent_collection)
      GetParentCollections(parent_collection, parent_names)
      return

def CreateCollectionHierarchy(collection):
    found = RecursiveSearchLayerCollection(layer, collection_name)
    if found:
        print("---- Creating Node Hierarchy ----")
        print("---- for collection: {name} ----".format(name = collection))

def GetNestedObjectsFromCollection(collection, depth: int) -> []:
    objects = []
    # Direct objects of the collection.
    objects.extend(collection.objects)

    # Dive into other child collections
    for col in collection.children:
        nested_objects = GetNestedObjectsFromCollection(col, depth)
        objects.extend(nested_objects)

    return objects

def Find(name, path) -> str:
    for root, dirs, files in os.walk(path):
        for file in files:
            if name in file:
                return os.path.join(root, name) + ".gdt"
    return None
