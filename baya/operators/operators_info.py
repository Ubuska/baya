import bpy
import addon_utils
import re
import os, stat
import zipfile
import subprocess
import tempfile
import shutil

from distutils.version import StrictVersion


def change_permissions_recursive(path, mode):
    for root, dirs, files in os.walk(path, topdown=False):
        for dir in [os.path.join(root,d) for d in dirs]:
            os.chmod(dir, mode)
    for file in [os.path.join(root, f) for f in files]:
            os.chmod(file, mode)

#def register():
    #for cls in (ExportXModel):
    #bpy.utils.register_class(COD_OT_Update)

#def unregister():
    #for cls in (ExportXModel):
    #bpy.utils.unregister_class(COD_OT_Update)


#if __name__ == "__main__":
    #register()
