import bpy
from bpy.props import IntProperty
from bpy.props import StringProperty

import re
import os
import bmesh

import sys

if 'DEBUG_MODE' in sys.argv:
    from utils import *
    from utils import data_types as dt
    from utils import generic_functions as g
else:
    from ..utils import *
    from ..utils import data_types as dt
    from ..utils import generic_functions as g

debug = False

class BAYA_OT_SelectMaterial(bpy.types.Operator):
    bl_idname = "baya.material_select"
    bl_label = "Select"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        settings = context.scene.material_manager_settings
        return True

    def execute(self, context):
        settings = context.scene.material_manager_settings
        material = bpy.data.materials[self.index]
        name = material.name
        print("Material %s selected." % name)
        return {'FINISHED'}

class BAYA_OT_DeleteMaterial(bpy.types.Operator):
    bl_idname = "baya.material_delete"
    bl_label = "Remove"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        settings = context.scene.material_manager_settings
        return True

    def execute(self, context):
        settings = context.scene.material_manager_settings
        material = bpy.data.materials[self.index]
        name = material.name
        bpy.data.materials.remove(material)
        print("Material %s removed." % name)
        return {'FINISHED'}

class BAYA_OT_CleanupMaterials(bpy.types.Operator):
    bl_idname = "baya.materials_cleanup"
    bl_label = "Purge unused materials"
    bl_options = set()

    def execute(self, context):
        print("Removing all unused materials...")
        settings = context.scene.material_manager_settings

        for material in bpy.data.materials:
            if not material.users:
                bpy.data.materials.remove(material)
        return {'FINISHED'}

class BAYA_OT_AssignMaterial(bpy.types.Operator):
    bl_idname = "baya.material_assign"
    bl_label = "Assign"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        if len(bpy.context.selected_objects) > 0:
            for obj in bpy.context.selected_objects:
                if obj.type == 'MESH':
                    return True
        return False

    def execute(self, context):
        settings = context.scene.material_manager_settings
        settings.active_material_index
        material = bpy.data.materials[self.index]

        active_object = bpy.context.view_layer.objects.active
        if active_object:
            current_mode = active_object.mode
            if current_mode == 'OBJECT':
                active_object.data.materials.clear()
                active_object.data.materials.append(material)
                print("Assigning material in Object mode.")
            elif current_mode == 'EDIT':
                material_index = -1
                if material.name not in active_object.data.materials:
                    active_object.data.materials.append(material)

                for index, material_slot in enumerate(active_object.data.materials):
                    if material_slot.name == material.name:
                        material_index = index

                bm = bmesh.from_edit_mesh(active_object.data)
                polyIdxs = [ f.index for f in bm.faces if f.select ]
                for idx in polyIdxs:
                    bm.faces[idx].material_index = material_index
                bmesh.update_edit_mesh(active_object.data)
                print("Assigning material in Edit mode.")
        #
        return {'FINISHED'}


class BAYA_OT_ReassignMaterial(bpy.types.Operator):
    bl_idname = "baya.material_reassign"
    bl_label = "Reassign one material to another."
    bl_options = set()

    @classmethod
    def poll(cls, context):
        if context.scene.material_reassign_source == context.scene.material_reassign_target:
            return False
        if context.scene.material_reassign_source and context.scene.material_reassign_target:
            return True

    def execute(self, context):
        settings = context.scene.material_manager_settings
        material_source_name = context.scene.material_reassign_source
        material_target_name = context.scene.material_reassign_target

        material_source = None
        material_target = None
        for material in bpy.data.materials:
            if material.name == material_source_name:
                material_source = material
            elif material.name == material_target_name:
                material_target = material

        if material_source_name and material_target_name:
            if material_source and material_target:
                print("Reassigning material [{name_source}] to [{name_target}].".format(name_source = material_source_name, name_target = material_target_name))

                for object in bpy.data.objects:
                    if object.type == 'MESH' or object.type == 'TEXT':
                        if len(object.data.materials) > 0:
                            for idx, mat in enumerate(object.data.materials):
                                if mat:
                                    if mat.name == material_source_name:
                                        object.data.materials[idx] = material_target

                # Delete material if needed.
                if context.scene.material_reassign_delete:
                    bpy.data.materials.remove(material_source)
                context.scene.material_reassign_source = ""

        return {'FINISHED'}


def register():
    for cls in (BAYA_OT_AssignMaterial, BAYA_OT_ReassignMaterial, BAYA_OT_SelectMaterial, BAYA_OT_DeleteMaterial, BAYA_OT_CleanupMaterials):
        bpy.utils.register_class(cls)


def unregister():
    for cls in (BAYA_OT_AssignMaterial, BAYA_OT_ReassignMaterial, BAYA_OT_SelectMaterial, BAYA_OT_DeleteMaterial, BAYA_OT_CleanupMaterials):
        bpy.utils.unregister_class(cls)
