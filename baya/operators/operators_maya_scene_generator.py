# Blender - Maya scene converter. Developed by Peter Gubin.

import bpy
import sys
import os
import subprocess
import itertools
import bmesh
import copy
import array
import mathutils
import math
import datetime
from math import pi
from mathutils import Vector
from enum import Enum
from bpy.props import StringProperty, BoolProperty, EnumProperty, FloatProperty
from bpy.types import Operator
from bpy_extras.io_utils import ExportHelper
from bpy_extras.view3d_utils import location_3d_to_region_2d
from bpy.types import PropertyGroup


if 'DEBUG_MODE' in sys.argv:
    from utils import *
    from utils import data_types as dt
    from utils import generic_functions as g
else:
    from ..utils import *
    from ..utils import data_types as dt
    from ..utils import generic_functions as g

debug = False


class GeneratorOptions(bpy.types.PropertyGroup):

    def transform_action_changed(self, context):
        print(self, self.transform_action, context)

    transform_action : bpy.props.EnumProperty(
    name="Transform Action",
    items=(
        ('Persist', 'Persist', 'Leave transform values intact.'),
        ('Freeze', 'Freeze', 'Apply transform values.'),
        ('Reset', 'Reset', 'Reset transform values'),
    ),
    default="Persist",
    update=transform_action_changed)



    # Options.
    export_only_used_materials : bpy.props.BoolProperty(name='Export only Materials in use', default=False)
    treat_vc_channel_as_alpha : bpy.props.BoolProperty(name='Export separate Vertex Color Channel As Alpha', default=False)

    replicate_viewport : bpy.props.BoolProperty(name='Replicate Viewport', default=True)

    auto_rename_uvs : bpy.props.BoolProperty(name='Auto rename UVs', default=True)
    auto_correct_names : bpy.props.BoolProperty(name='Auto correct names', default=True)
    rename_last_lod : bpy.props.BoolProperty(name='Rename last lod to LODX', default=True)

    move_to_world_origin : bpy.props.BoolProperty(name='Move to world origin', default=False)
    clear_scale : bpy.props.BoolProperty(name='Clear Scale', default=True)
    clear_rotation : bpy.props.BoolProperty(name='Clear Rotation', default=False)

    # UI.
    export_options : bpy.props.BoolProperty(name='Baya Options', default=False)

    # Export.
    source_absolute_path : bpy.props.StringProperty(name='Source File Absolute Path', default="C:/", subtype='DIR_PATH')
    file_name : bpy.props.StringProperty(name='File Name', default="converted_maya_file_name")

    export_scale : bpy.props.FloatProperty(name='Scale', default = 1.0)

class Type(Enum):
    Short               = 1
    String              = 2

class Node(Enum):
    Transform       = 1
    Mesh            = 2
    Phong           = 3
    Material        = 4
    Shading         = 5
    RenderLayer     = 6
    Group           = 7
    Camera          = 8
    HavokRigidbody  = 9
    HavokShape      = 10

class SceneInfo:

    materials = [] # Materials that need to be exported.
    objects = [] # Objects that need to be exported.

class Vector3:
    x : float
    y : float
    z : float
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def ToString(self) -> str:
        return str(self.x) + " " + str(self.y) + " " + str(self.z)

class Log(object):
    @staticmethod
    def Print(message:str):
        print("Maya Converter:: " + message)

def GetSourcePath() -> str:
    return bpy.context.scene.baya_options.source_absolute_path


class BaseNode(object):
    parent = None
    name: str
    output = None

    def __init__(self, name: str):
        self.name = name

    def Clean(self):
        pass

    def VGetNodeTypeName(self) -> str:
        return None;

    def Write(self):
        pass
        #Log.Print("[{name}] BaseNode.Write()".format(name=self.name))

    def WriteHeader(self):
        type_name = self.VGetNodeTypeName()
        node_name = self.name
        debug_header = "\n"
        header = debug_header + 'createNode {type} -n "{name}"'.format(type=type_name, name=GetName(node_name))

        if self.parent:
            header += ' -p "{parent}"'.format(parent = self.parent)
        self.output.WriteLine(header)

    def SetAttribute(self, text: str, debug_text: str = None):
        if debug_text:
            self.output.WriteLine("\n\n // " + debug_text)
        self.output.WriteLine("setAttr " + text)

    def AddAttribute(self, name: str, type: Type, debug_text: str = None):
        type_prefix: str = "TypePrefix"
        type_name: str = "TypeName"
        if type == Type.Short:
            type_prefix = "at"
            type_name = "short"
        elif type == Type.String:
            type_prefix = "dt"
            type_name = "string"
        text = '-sn "{name}" -ln "{name}" -{tprefix} "{type}"'.format(name = GetName(name), tprefix = type_prefix, type = GetName(type_name))
        if debug_text:
            self.output.WriteLine("\n\n // " + debug_text)
        self.output.WriteLine("addAttr " + text)

    def SetParent(self, parent):#: BaseNode):
        self.parent = parent

# Base class for all Maya scene nodes.
# Should not be instantiated directly.

class SceneNode(BaseNode):

    def Write(self):
        BaseNode.Write(self)

    def Clean(self):
        BaseNode.Clean(self)

        self.object = None
        del self.object

class TransformNode(SceneNode):
    location    : Vector3
    rotation    : Vector3
    scale       : Vector3
    visible     = True

    def __init__(self, name: str):
        super(SceneNode, self).__init__(name)
        self.location = Vector3(0, 0, 0)
        self.rotation = Vector3(0, 0, 0)
        self.scale =    Vector3(1, 1, 1)

    def VGetNodeTypeName(self) -> str:
        return "transform";

    def Write(self):
        SceneNode.Write(self)

        self.SetAttribute('".t" -type "double3" ' + self.location.ToString())
        self.SetAttribute('".r" -type "double3" ' + self.rotation.ToString())
        self.SetAttribute('".s" -type "double3" ' + self.scale.ToString())
        if not self.visible:
            self.SetAttribute('".v" no')
        super(SceneNode, self).Write()

class GeometryData:
    header = ""
    coordinates = ""

    def __init__(self, header: str, coordinates: str):
        self.header = header
        self.coordinates = coordinates

def GenerateUVIndexArray(uvs) -> []:
    print()
    print("Generating UV Index Array.")
    index_array = []

    for index, uv in enumerate(uvs):
        for index, uv_data in enumerate(uv.data):
            print("UV Coord {index}: [{x}] [{y}]".format(index = index, x = uv_data.uv.x, y = uv_data.uv.y))
            if uv_data.uv.x not in index_array:
                index_array.append(uv_data.uv.x)
            if uv_data.uv.y not in index_array:
                index_array.append(uv_data.uv.y)

    return index_array

def Ranges(p):
    q = sorted(p)
    i = 0
    for j in range(1,len(q)):
        if q[j] > 1+q[j-1]:
            yield (q[i],q[j-1])
            i = j
    yield (q[i], q[-1])

def GetName(name: str) -> str:
    new_name = copy.copy(name)
    converted_name = new_name.replace('.', '_')
    converted_name = converted_name.replace(' ', '_')

    return converted_name

def RGBtoGrayscale(r, g, b):
    return 0.2989 * r + 0.5870 * g + 0.1140 * b

class MeshNode(SceneNode):

    object = None
    duplicate = False

    def VGetNodeTypeName(self) -> str:
        return "mesh";

    def string_from_vector(self, vec) -> str:
        return "{x} {y} {z}".format(x=vec[0], y=vec[1], z=vec[2])

    def Write(self):
        """
        Write the Mesh (Geometry) data block.
        """
        context = bpy.context
        scene = context.scene

        linked = False

        master_collection = bpy.context.scene.collection
        original_object = self.object
        duplicated_object = None

        if self.object is None:
            return

        if len(self.object.modifiers) > 0 or self.object.instance_type == 'COLLECTION':
            duplicated_object = self.object.copy()  # duplicate linked
            duplicated_object.name = self.object.name + "___"
            if self.object.instance_type != 'COLLECTION':
                duplicated_object.data = self.object.data.copy()


            bpy.ops.object.select_all(action='DESELECT')

            # Link it with collection.
            master_collection.objects.link(duplicated_object)
            bpy.context.view_layer.objects.active = duplicated_object
            duplicated_object.select_set(state=True)

            if self.object.instance_type == 'COLLECTION':
                bpy.ops.baya.collapse_object('INVOKE_DEFAULT')
                self.object = bpy.context.view_layer.objects.active
                for mat_slot in self.object.material_slots:
                    mat_slot.material = mat_slot.material.copy()
                linked = True
            else:
                for mod in duplicated_object.modifiers:
                    bpy.ops.object.modifier_apply(modifier = mod.name)

                self.object = duplicated_object

        vertex_indices = []
        uv_attributes = []
        uv_indices = []
        vertex_color_attributes = []
        vertex_color_indices = []
        materials = []

        vertex_color_rgb = []
        vertex_color_alpha = []

        SceneNode.Write(self)
        self.SetAttribute('-k off ".v"')
        self.SetAttribute('".vir" yes')
        self.SetAttribute('".vif" yes')

        vertex_coords = [(v.co) for v in self.object.data.vertices]
        vertex_colors = self.object.data.vertex_colors
        uvs = self.object.data.uv_layers
        faces = self.object.data.polygons
        loops = self.object.data.loops
        edges = self.object.data.edges
        vertices = self.object.data.vertices
        key_edges = {ed.key: ed.index for ed in self.object.data.edges}

        # Material IDs.
        material_polygons = { ms.material.name : [] for ms in self.object.material_slots if ms.material != None}

        if len(self.object.material_slots) > 0:
            for i, p in enumerate( faces ):
                if self.object.data.materials[p.material_index]:
                    material_polygons[ self.object.material_slots[ p.material_index ].name ].append( i )

        for index, material in enumerate(self.object.data.materials):
            if material:
                l = material_polygons[material.name]
                ranges = Ranges(l)
                mat_ids = ""
                count = 0

                for idx, r in enumerate(ranges):
                    mat_ids += ' "f[{first}:{last}]"'.format(first = r[0], last = r[len(r)-1])
                    count += 1
                materials.append('".iog[0].og[{mat_index}].gcl" -type "componentList" {count} {mat_ids}'.format(mat_index = index, mat_ids = mat_ids, count = count))

            # @Todo: Add initial material to empty material slots.
            #else:
        # Vertex color.

        vchs = []
        vcds = []

        # Vertex positions.
        vt = '-s {count} ".vt[{first}:{last}]"'.format(count=len(vertex_coords), first=0, last=len(vertex_coords)-1)
        vt += "\n"
        for v in vertex_coords:
            vt += self.string_from_vector(v) + "  \n"

        # Edges.
        ed = '-s {count} ".ed[{first}:{last}]" \n'.format(count=len(edges), first=0, last=len(edges)-1)
        for e in edges:
            ed += " {v1} {v2} {v3} \n".format(v1=e.vertices[0], v2=e.vertices[1], v3=0)

        key_edges = {ed.key: ed.index for ed in self.object.data.edges}

        # Vertex normals.
        self.object.data.calc_normals_split()
        for idx, face in enumerate(faces):
            for idx, vertex in enumerate(face.vertices):
                vertex_indices.append(vertex)

        vn = '-s {count} ".n[{first}:{last}]" -type "float3" \n'.format(count=len(vertex_indices), first=0, last=len(vertex_indices)-1)
        for index, vertex_index in enumerate(vertex_indices):
            normal = self.object.data.loops[index].normal
            vn += self.string_from_vector(normal) + "  \n"

        # Write UV layers.
        # Note: LayerElementTexture is deprecated since FBX 2011 - luckily!
        #       Textures are now only related to materials, in FBX!

        if context.scene.baya_options.treat_vc_channel_as_alpha:
            if len(vertex_colors) > 1:
                print(" ---           treat_vc_channel_as_alpha is enabled")
                print(" There is second Vertex Color channel. TREATING AS ALPHA.")
                color_layer = self.object.data.vertex_colors[0]
                alpha_layer = self.object.data.vertex_colors[1]

                for i, vertex_color in enumerate(color_layer.data):
                    alpha = RGBtoGrayscale(alpha_layer.data[i].color[0], alpha_layer.data[i].color[1], alpha_layer.data[i].color[2])
                    vertex_color.color[3] = alpha

        # Indexing vertex colors.
        vcnumber = len(vertex_colors)
        if vcnumber:
            def _vctuples_gen(raw_vcs):
                return zip(*(iter(raw_vcs),) * 4)

            t_lvc = array.array(dt.ARRAY_FLOAT64, (0.0,)) * len(loops) * 4

            print("\n\n ----------------------------- Indexing vertex colors -----------------------------\n\n")



            for vcindex, vclayer in enumerate(vertex_colors):
                if context.scene.baya_options.treat_vc_channel_as_alpha:
                    if vcindex > 0:
                        break
                vc_layer_name = vclayer.name
                if context.scene.baya_options.auto_correct_names:
                    vc_layer_name = "vc" + str(vcindex + 1)

                header = '".clst[{index}].clsn" -type "string" "{name}"'.format(index = vcindex, name = vc_layer_name)
                vclayer.data.foreach_get("color", t_lvc)
                vc2idx = tuple(set(_vctuples_gen(t_lvc)))

                coordinates = '-s {count} ".clst[{index}].clsp[{first}:{last}]" -type "string" '.format(count = len(vc2idx), index = vcindex, first = 0, last = len(vc2idx) - 1)
                for vcidx in vc2idx:
                    coordinates += "\n{R} {G} {B} {A}".format(R = vcidx[0], G = vcidx[1], B = vcidx[2], A = vcidx[3])

                #print("    ---     Coordinates for [{idx}] index...".format(idx = vcindex))
                #print(coordinates)

                new_vc = GeometryData(header, coordinates)
                vertex_color_attributes.append(new_vc)
                vertex_color_indices.append(vc2idx)

        # Indexing uv coordinates.
        uvnumber = len(uvs)
        if uvnumber:
            def _uvtuples_gen(raw_uvs):
                return zip(*(iter(raw_uvs),) * 2)

            t_luv = array.array(dt.ARRAY_FLOAT64, (0.0,)) * len(loops) * 2

            for uvindex, uvlayer in enumerate(uvs):
                uv_layer_name = uvlayer.name
                if bpy.context.scene.baya_options.auto_rename_uvs:
                    uv_layer_name = "map" + str(uvindex + 1)
                header = '".uvst[{index}].uvsn" -type "string" "{name}"'.format(index = uvindex, name = uv_layer_name)
                uvlayer.data.foreach_get("uv", t_luv)
                uv2idx = tuple(set(_uvtuples_gen(t_luv)))

                coordinates = '-s {count} ".uvst[{index}].uvsp[{first}:{last}]" -type "float2" '.format(count = len(uv2idx), index = uvindex, first = 0, last = len(uv2idx) - 1)
                for uvidx in uv2idx:
                    coordinates += "\n{x} {y}".format(x = uvidx[0], y = uvidx[1])

                new_uv = GeometryData(header, coordinates)
                uv_attributes.append(new_uv)
                uv_indices.append(uv2idx)

        # Faces.
        last = ""
        if len(faces) > 1:
            last = str(len(faces) - 1)
        fc = '-s {fcount} ".fc[{first}:{last}]" -type "polyFaces" \n'.format(fcount=len(faces), first=0, last=last)
        for f in faces:
            fc += " f {faces_count}".format(faces_count = f.loop_total)

            # Writing face edges info.
            for edge in f.edge_keys:
                fc += " {edge}".format(edge=str(key_edges.get(edge)))

            fc += "\n"

            # Writing UV Channels info.
            for uvindex, uvlayer in enumerate(uvs):
                fc += "mu {index} {indices_count}".format(index = uvindex, indices_count = len(f.vertices))
                for vert, loop in zip(f.vertices, f.loop_indices):
                    item = uvs[uvindex].data[loop].uv
                    for index, uv in enumerate(uv_indices[uvindex]):
                        if uv[0] == item.x and uv[1] == item.y:
                            fc += " {index}".format(index = index)
                            break
                #fc += " // UV channel [{index}] \n".format(index = uvindex)

            fc += "\n"

            # Writing Vertex Color info.
            for vcindex, vc in enumerate(vertex_colors):
                if context.scene.baya_options.treat_vc_channel_as_alpha:
                    if vcindex > 0:
                        break
                vertex_index = 0
                #print("--- --- --- --- Reading indexes for [{idx}] vertex color --- --- --- ---\n".format(idx = vcindex))
                #fc += "mc {index} {indices_count}".format(index = vcindex, indices_count = len(vertex_colors.data.vertices))
                fc += "mc {index} {indices_count}".format(index = vcindex, indices_count = len(f.vertices))
                for vert, loop in zip(f.vertices, f.loop_indices):
                    #print("--- --- Vertex {index}:".format(index = vertex_index))
                    item = vertex_colors[vcindex].data[loop].color
                    vertex_index += 1

                    for index, vcol in enumerate(vertex_color_indices[vcindex]):
                        #print("Comparing index : [{idx}] : {R} {G} {B} {A}".format(idx = index, R = vcol[0], G = vcol[1], B = vcol[2], A = vcol[3]))
                        if vcol[0] == item[0] and vcol[1] == item[1] and vcol[2] == item[2] and vcol[3] == item[3]:
                            # if context.scene.baya_options.treat_vc_channel_as_alpha:
                            #     if len(vertex_colors) > 1:
                            #         print(" !!! !!!           treat_vc_channel_as_alpha is enabled           !!! !!! ")
                            #         print(" !!! !!! There is second Vertex Color channel. TREATING AS ALPHA. !!! !!! ")

                            fc += " {index}".format(index = index)
                            #print("           took : [{idx}]".format(idx = index))
                            break
                    #fc += " {index}".format(index = 0)
                fc += " \n".format(index = vcindex)

        # Writing materials.
        for mat_id in materials:
            #print("           object - " + self.object.name + ":")
            #("            -- writing attribute" + str(mat_id))
            self.SetAttribute(mat_id)

        # ----------------------------- Writing attributes ----------------------------

        # Writing UVs.
        for uv_channel_index, uv_attribute in enumerate(uv_attributes):
            #self.output.WriteLine('\n// UV Channel [{index}]'.format(index = uv_channel_index))
            self.SetAttribute(uv_attribute.header)
            self.SetAttribute(uv_attribute.coordinates)

        if vertex_colors:
            self.SetAttribute('".dcol" yes', "Vertex color enabled")
            # Writing Vertex Colors.
            for vertex_color_attribute in vertex_color_attributes:
                #self.output.WriteLine('\n// Vertex Color Layer [{index}]'.format(index = vertex_color_index))
                self.SetAttribute(vertex_color_attribute.header)
                self.SetAttribute(vertex_color_attribute.coordinates)
                #self.SetAttribute(vertex_color_attribute.header, "Vertex color header")
                #self.SetAttribute(vertex_color_attribute.coordinates, "Vertex color data")

        self.SetAttribute(vt, "Vertices")
        self.SetAttribute(ed, "Edges")
        self.SetAttribute(vn, "Vertex Normals")
        self.SetAttribute(fc, "Face Info")

        vertex_coords.clear()
        vertex_indices.clear()
        uv_attributes.clear()
        uv_indices.clear()
        vertex_color_indices.clear()
        vertex_color_attributes.clear()
        materials.clear()
        vchs.clear()
        vcds.clear()

        if duplicated_object:
            bpy.ops.object.select_all(action='DESELECT')
            if not linked:
                bpy.context.view_layer.objects.active = duplicated_object
                duplicated_object.select_set(state=True)
                bpy.ops.object.delete()

            self.object = original_object

    def Clean(self):
        BaseNode.Clean(self)
        if self.duplicate:
            bpy.ops.object.select_all(action='DESELECT')
            self.object.select_set(state=True)
            bpy.ops.object.delete()

class PhongNode(BaseNode):

    def VGetNodeTypeName(self) -> str:
        return "phong";

    def Write(self):
        BaseNode.Write(self)

class MaterialNode(BaseNode):

    def VGetNodeTypeName(self) -> str:
        return "dx11Shader";

    def Write(self):
        BaseNode.Write(self)

class ShadingNode(BaseNode):

    def VGetNodeTypeName(self) -> str:
        return "shadingEngine";

    def Write(self):
        BaseNode.Write(self)

class GroupIdNode(BaseNode):

    def VGetNodeTypeName(self) -> str:
        return "groupId";

    def Write(self):
        BaseNode.Write(self)

class CameraNode(BaseNode):

    focal_length = 40.0
    near_clipping_plane = 0.05
    far_clipping_plane = 4000.0

    def VGetNodeTypeName(self) -> str:
        return "camera";

    def Write(self):
        BaseNode.Write(self)
        self.SetAttribute('".fl" {focal_length}'.format(focal_length = self.focal_length), "Focal Length")
        self.SetAttribute('".ncp" {value}'.format(value = self.near_clipping_plane), "Near Clipping Plane")
        self.SetAttribute('".fcp" {value}'.format(value = self.far_clipping_plane), "Far Clipping Plane")

class RenderLayerNode(BaseNode):

    def VGetNodeTypeName(self) -> str:
        return "renderLayer";

    def Write(self):
        BaseNode.Write(self)

class SceneOutput(object):

    file = None
    nodes = []
    mesh_nodes = []
    material_nodes = []
    scene_info = SceneInfo()

    def WriteLine(self, text: str):
        self.file.write(text + ";\n")

    def WriteSceneFile(self, file_name: str):
        source_path = GetSourcePath()
        if not os.path.exists(source_path):
            os.makedirs(source_path)

        print(".......................................  Printing scene file  .........................................\n")


        filepath = '{filepath}\\{filename}.ma'.format(filepath=source_path, filename=file_name)
        self.file = open(filepath, 'w', encoding='utf-8')

        print("..  Generating Maya ASCII file: {W}  ..\n".format(W=filepath))

        print("- Header - ")
        self.WriteHeader(file_name)
        print("- Description - ")
        self.WriteDescription(file_name)

        # - - - - - - - - - Nodes creation  - - - - - - - - -

        written_nodes = []

        print("- Objects - ")
        # Write objects info.
        for node in self.nodes:
            if isinstance(node, MeshNode) or isinstance(node, TransformNode):
                self.WriteNode(node)
                written_nodes.append(node)

        remaining_nodes = (n for n in self.nodes if n not in written_nodes)
        for node in remaining_nodes:
            self.WriteNode(node)

        # - - - - - - - - - Attribute connections  - - - - - - - - -

        print("- Attribute connections - ")
        # Connect groups and shapes

        for mesh_node in self.mesh_nodes:
            if mesh_node.object and mesh_node and mesh_node.object.data:
                if isinstance(mesh_node, MeshNode):

                    # If mesh don't have any materials assigned to it.
                    if len(mesh_node.object.data.materials) == 0:
                        print("               No material, connecting default material.")
                        self.ConnectAttribute("{name}.iog".format(name = GetName(mesh_node.object.name) + "Shape"), "initialShadingGroup.dsm", "-na")

                    for index, material in enumerate(mesh_node.object.data.materials):
                        if material:
                            print("               Material [{slot}]: ".format(slot = index) + material.name)
                            GroupIndex = self.scene_info.materials.index(material)
                            self.ConnectAttribute("groupId{index}.id".format(index = GroupIndex+1), "{name}Shape.iog.og[{idx}].gid".format(idx = index, name = GetName(mesh_node.object.name)))
                            self.ConnectAttribute("{name}.oc".format(name = GetName(material.name)), "{name}SG.ss".format(name = GetName(material.name)))
                            self.ConnectAttribute("{name}.iog.og[{index}]".format(name = GetName(mesh_node.object.name) + "Shape", index = index), "{name}SG.dsm".format(name = GetName(material.name)), "-na")

                        # Material slot is empty.
                        else:
                            print("               Material [{slot}]: <no material, connecting default material>".format(slot = index))

                            GroupIndex = index
                            self.ConnectAttribute("groupId{index}.id".format(index = GroupIndex+1), "{name}Shape.iog.og[{idx}].gid".format(idx = index, name = GetName(mesh_node.object.name)))
                            self.ConnectAttribute("{name}.iog.og[{index}]".format(name = GetName(mesh_node.object.name) + "Shape", index = index), "initialShadingGroup.dsm", "-na")

        # New material to shader list (note for future).
        #self.ConnectAttribute("lambert.oc", "lambertSG1.ss")
        #self.ConnectAttribute("lambert.msg", ":defaultShaderList1.s", "-na")
        #self.ConnectAttribute("defaultRenderLayer.msg", ":defaultRenderingList1.r")

        # Add materials to shader list.
        for material in self.material_nodes:
            self.ConnectAttribute("{name}.msg".format(name = material.name), ":defaultShaderList1.s", "-na")

        print("- Footer - ")
        self.WriteFooter()

        self.file.close()

        for node in self.nodes:
            node.Clean()

        self.nodes.clear()
        self.mesh_nodes.clear()
        self.material_nodes.clear()


    def WriteNode(self, node: BaseNode):
        node.WriteHeader()
        node.Write()

    def GetNode(self, name: str) -> BaseNode:
        el = [x for x in self.nodes if x.name == name]
        if el: return el[0]

        return None

    def CreateNode(self, name: str, type: Node = Node.Transform) -> BaseNode:

        for n in self.nodes:
            if n.name == name:
                return n

        node : BaseNode = None

        if debug:
            print("Debug: CreateNode [{type}] : [{name}].".format(name = name, type = str(type)))

        if type == Node.Transform:
            #print(GetName(name))
            node = TransformNode(GetName(name))
        elif type == Node.Mesh:
            node = MeshNode(GetName(name))
            self.mesh_nodes.append(node)
        elif type == Node.Phong:
            node = PhongNode(GetName(name))
            self.material_nodes.append(node)
        elif type == Node.Material:
            node = MaterialNode(GetName(name))
        elif type == Node.Shading:
            node = ShadingNode(GetName(name))
        elif type == Node.RenderLayer:
            node = RenderLayerNode(GetName(name))
        elif type == Node.Group:
            node = GroupIdNode(GetName(name))
        elif type == Node.Camera:
            node = CameraNode(GetName(name))
        elif type == Node.HavokRigidbody:
            node = HavokRigidbodyNode(GetName(name))
        elif type == Node.HavokShape:
            node = HavokShapeNode(GetName(name))
        else:
            print("Error: unknown node type.")

        node.output = self
        self.nodes.append(node)

        return node

    def ConnectAttribute(self, Attribute1: str, Attribute2 : str, Modifier : str = ""):
        AttributeConnection = 'connectAttr "{Atr1}" "{Atr2}" {Mod}'.format(Atr1=Attribute1, Atr2=Attribute2, Mod=Modifier)
        self.WriteLine(AttributeConnection)

    ## Write general information.
    def WriteHeader(self, file_name: str):
        if self.file:
            today = datetime.date.today()
            self.file.write("//Maya ASCII 2018ff07 scene \n")
            self.file.write("//Name: {name}.ma \n".format(name = file_name))
            #self.file.write("//Last modified: {date_time} \n".format(date_time = "Wed, Feb 20, 2019 08:16:34 PM"))
            self.file.write("//Codeset: 1251 \n")

            self.file.write('requires maya "2018ff07";\n')
            #self.file.write('requires -nodeType "hkNodeShape" -nodeType "hkNodeRigidBody" "hctMayaSceneExport" "2018.1.0.1 (2018.1 r1)";\n')
            self.file.write('requires "mtoa" "3.0.1.1";\n')
            self.file.write("currentUnit -l inch -a degree -t ntsc;\n")

    ## Write file description.
    def WriteDescription(self, file_name: str):
        if self.file:
            self.file.write('fileInfo "application" "maya"; \n')
            self.file.write('fileInfo "product" "Maya 2018";\n')
            self.file.write('fileInfo "version" "2018";\n')
            self.file.write('fileInfo "cutIdentifier" "201711281015-8e846c9074";\n')
            self.file.write('fileInfo "osv" "Microsoft Windows 10, 64-bit  (Build 9200)";\n')
            self.file.write('\n\n')

    def WriteFooter(self):
        self.WriteLine("// End of Test.ma")

class Maya_Generator_Panel(bpy.types.Panel):
    bl_label = "Baya ASCII Converter"
    bl_idname = "maya_generator.generator_panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Baya'

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        row = layout.row()
        row.operator("maya_generator.generate_maya_file")
        #row.operator("baya.open_source_folder")

        row = layout.row()
        row.prop(scene.baya_options, "file_name")
        row = layout.row()
        row.prop(scene.baya_options, "source_absolute_path")

        row = layout.row()
        row.operator("baya.open_export_folder")
        #row = layout.row()
        #row.operator("baya.setup_metrics")

        box = layout.box()
        if scene.baya_options.export_options:
            box.prop(scene.baya_options, "export_options", icon="TRIA_DOWN", emboss=False)
            box.prop(scene.baya_options, "export_scale")

            row = layout.row()
            box.prop(scene.baya_options, "transform_action", icon = 'MOD_MESHDEFORM')
            box.prop(scene.baya_options, "move_to_world_origin", toggle=True)


            #row = box.row()
            #row.prop(scene.baya_options, "export_vc_channel_as_alpha", toggle=True)

            # @Todo: temporarily disabled.
            #row = box.row()
            #row.prop(scene.baya_options, "export_only_used_materials", toggle=True)

            row = box.row()
            row.prop(scene.baya_options, "auto_rename_uvs", toggle=True)
            row.prop(scene.baya_options, "auto_correct_names", toggle=True)
            row.prop(scene.baya_options, "rename_last_lod", toggle=True)

            row = box.row()
            row.prop(scene.baya_options, "replicate_viewport", toggle=True)

            row = box.row()
            row.prop(scene.baya_options, "treat_vc_channel_as_alpha", text = "Separate Vertex Alpha Channel", toggle=True)



            row = box.row()
            box = row.box()
            if scene.baya_options.treat_vc_channel_as_alpha:
                box.label(text="only 2 vertex color channels!")
                box.label(text="will be exported!")
                box.label(text="First for RGB and second for Alpha.")
                box.label(text="If there is only 1 channel")
                box.label(text="Alpha will be stored as 1.0")
                box.label(text="Tint is ignored, only value is used")
            else:
                box.label(text="Unlimited RGBA vertex color channels")

        else:
            box.prop(scene.baya_options, "export_options", icon="TRIA_RIGHT", emboss=False)

class GenerateMayaFile(bpy.types.Operator):
    bl_idname = "maya_generator.generate_maya_file"
    bl_label = "Generate Maya File"
    bl_options = set()
    output = None
    context = None
    depth = 0
    written_objects = []

    def WriteObject(self, object, parent_node = None, duplicate = False, collection_node = None):

        self.written_objects.append(object)
        self.depth += 1
        tab = "-"
        i = 0
        while i < self.depth:
            tab += "---"
            i += 1

        print(tab + " " + object.name)
        if object.type == 'EMPTY' or object.type == 'MESH':

         #print("Setting relations for object nodes ({count})".format(count = str(len(written_objects))))
            # for object in master.objects:
            #     if object.parent:
            #         print("--- object '{object}' has parent '{parent}', setting parent relation...".format(object = object.name, parent = object.parent.name))
            #         object_child = None
            #         object_parent = None



            if object.display_type == 'TEXTURED' or object.display_type == 'SOLID':
                object_transform_node = self.output.CreateNode(GetName(object.name), Node.Transform)

                if parent_node:
                    object_transform_node.parent = parent_node.name

                    #print("--- object '{object}' has parent '{parent}', setting parent relation...".format(object = object.name, parent = object.parent.name))
                    ##print("--- (child) {child} --> {parent} (parent)".format(child = object.name, parent = object.parent.name))
                    #object_transform_node.parent = object.parent.name

                object_transform_node.location.x = object.location.x
                object_transform_node.location.y = object.location.y
                object_transform_node.location.z = object.location.z

                object_transform_node.rotation.x = math.degrees(object.rotation_euler.x)
                object_transform_node.rotation.y = math.degrees(object.rotation_euler.y)
                object_transform_node.rotation.z = math.degrees(object.rotation_euler.z)

                #if not bpy.context.scene.baya_clear_scale:
                object_transform_node.scale.x = object.scale.x
                object_transform_node.scale.y = object.scale.y
                object_transform_node.scale.z = object.scale.z

                object_transform_node.visible = object.visible_get ()

                if collection_node:
                    object_transform_node.parent = collection_node.name

                if object.type == 'MESH' or object.instance_type == 'COLLECTION':
                    #mesh_node = output.CreateNode(object.name, Node.Transform)
                    mesh = self.output.CreateNode(GetName(object.name) + "Shape", Node.Mesh)
                    mesh.duplicate = duplicate
                    mesh.object = object
                    mesh.parent = object_transform_node.name

                if object.children:
                    for child in object.children:
                        self.WriteObject(child, object_transform_node)


                return object_transform_node

# @Robustify: Remove unnecesary stuff, simplify this function.
    def WriteCollection(self, collection, parent = None, skip_children = False):

        print(" --- --- Writing a collection ({name})".format(name = collection.name))
        if parent:
            print(" --- --- --- Parent : {parent}".format(parent = parent.name))
        collection_node = None

        # Check if this collection already exists.
        # If it exists, no need to create another one, just return existing one.

        found_collection = g.FindCollection(collection.name)
        if found_collection:
            # Find created node to return.
            for node in self.output.nodes:
                if node.name == found_collection.name:
                    if debug:
                        print("found [{name}] collection node, skip creation.".format(name = collection.name))
                    collection_node = node
                    break

        if collection_node == None:
            #if debug:
            print("creating [{name}] collection node. ".format(name = collection.name))
            collection_node = self.output.CreateNode(collection.name, Node.Transform)
            if parent:
                collection_node.parent = parent.name

        #print(collection.hide_viewport)
        collection_node.visible = not collection.hide_viewport

        if parent:
            collection_node.parent = parent.name

        # Write collection objects.
        objects = collection.objects

        print("                     Searching for objects in the collection '{collection}'".format(collection = collection.name))
        for object in objects:
            print("                    trying object '{object}'".format(object = object.name))

            is_already_written = False
            for written_object in self.written_objects:
                print("                            written object : " + written_object.name)
                if written_object.name == object.name:
                    is_already_written = True
                    break

            if not is_already_written:
                self.WriteObject(object, collection_node)



        # After all possible nodes generated, hook up parent nodes.
        # self.output.GetNode("name")

            #elif obj_type == 'EMPTY':
                #print("{tab} .[empty]{name}".format(tab = tab, name = object.name))

        #if skip_children == False:

        # Iterate further into children collections.
        for child in collection.children:
            self.WriteCollection(child, collection)

        return collection_node

    def GenerateNodes(self, context):

        print(".........................................  Generating nodes  .........................................\n")

        scene = context.scene

        # Camera.
        # @Todo: Fix, don't work atm?
        if scene.baya_options.replicate_viewport:
            camera_transform = self.output.CreateNode("persp", Node.Transform)

            camera_shape = self.output.CreateNode("perspShape", Node.Camera)
            camera_shape.parent = camera_transform.name


            for area in bpy.context.screen.areas:
                if area.type == 'VIEW_3D':
                    r3d = area.spaces.active.region_3d
                    tr = r3d.view_matrix.inverted().translation
                    oev = Vector(r3d.view_rotation.to_euler())
                    rt = r3d.view_rotation.to_euler()
                    scale = bpy.context.scene.baya_options.export_scale
                    location = Vector3(tr.x * scale, tr.y * scale, tr.z * scale)
                    rotation = Vector3(math.degrees(rt.x), math.degrees(rt.y), math.degrees(rt.z))
                    camera_transform.location = location
                    camera_transform.rotation = rotation

                    camera_shape.focal_length = area.spaces.active.lens
                    camera_shape.near_clipping_plane = area.spaces.active.clip_start
                    camera_shape.far_clipping_plane = area.spaces.active.clip_end

        # Iterate through collections.
        # Writing objects info.
        master = scene.collection

        # @Todo: Skip collections that are assigned as Havok containers.





        print("Export models mode: All data.")

        print("Collecting objects hierarchy...\n\n")

        for object in master.objects:
            if not object.parent and len(object.children):
                self.depth = 0
                self.WriteObject(object)
            elif not object.children and not object.parent:
                self.depth = 0
                self.WriteObject(object)
                #childless.append(object)

        for child in master.children:
            depth = 0
            self.WriteCollection(child)

        self.written_objects.clear()



        print("object hirerachy ends\n")



        #
        #
        # for object in master.objects:
        #
        #     written_object = self.WriteObject(object)
        #     written_objects.append(written_object)
        #
        # for child in master.children:
        #     depth = 0
        #     self.WriteCollection(child)

        # print("Setting relations for object nodes ({count})".format(count = str(len(written_objects))))
        # for object in master.objects:
        #     if object.parent:
        #         print("--- object '{object}' has parent '{parent}', setting parent relation...".format(object = object.name, parent = object.parent.name))
        #         object_child = None
        #         object_parent = None


        # # Create dummy material.
        # if context.scene.baya_options.use_dummy_material:
        #     self.output.CreateNode("lambert", Node.Phong)
        #     self.output.CreateNode("lambertSG", Node.Shading)

        for index, material in enumerate(self.output.scene_info.materials):
            self.output.CreateNode(material.name, Node.Phong)
            self.output.CreateNode(material.name + "SG", Node.Shading)

        for index, material in enumerate(self.output.scene_info.materials):
            self.output.CreateNode("groupId{index}".format(index = index+1), Node.Group)

        # Select scene objects that have no parent and children.
        # obs = [o for o in scene.objects if not o.parent and len(o.children)]
        # for ob in obs:
        #     obj_type = getattr(ob, 'type', '')
        #     if obj_type == 'MESH':
        #         node = output.CreateNode(ob.name, Node.Transform)
        #         mesh = output.CreateNode(ob.name + "Shape", Node.Mesh)
        #         mesh.object = ob
        #         mesh.parent = node


    def execute(self, context):

        print("\n\n\n-----------------------------------------------  Start  -----------------------------------------------\n")


        # Export dialog
        self.context = context
        print("\n....................................  Collecting scene information  ....................................\n")

        self.output = SceneOutput()
        self.output.nodes.clear()
        self.output.scene_info.materials.clear()

        #if context.scene.baya_options.export_only_used_materials:
        for material in bpy.data.materials:
                self.output.scene_info.materials.append(material)

        self.GenerateNodes(context)
        self.output.WriteSceneFile(context.scene.baya_options.file_name)

        return {'FINISHED'}

class OpenExportFolder(bpy.types.Operator):
    bl_idname = "baya.open_export_folder"
    bl_label = "Open Export Folder"
    bl_options = set()

    def execute(self, context):
        FILEBROWSER_PATH = os.path.join(os.getenv('WINDIR'), 'explorer.exe')
        path = os.path.normpath(GetSourcePath())
        print("Opening export folder: " + path)

        if not os.path.exists(path):
            os.makedirs(path)
        if os.path.isdir(path):
            subprocess.run([FILEBROWSER_PATH, path])
        elif os.path.isfile(path):
            subprocess.run([FILEBROWSER_PATH, '/select,', os.path.normpath(path)])
        return {'FINISHED'}

# It can be a Collection or Mesh.
class BATA_OT_CollapseObject(bpy.types.Operator):
    bl_idname = "baya.collapse_object"
    bl_label = "Collapse Objects"
    bl_options = set()

    # Collapse Collection, including nested Collection.
    def CollapseCollection(self, linked_collection_object):
        objects = []
        collection_empties = []

        if linked_collection_object:
            if linked_collection_object.type == 'EMPTY':
                if linked_collection_object.instance_type == 'COLLECTION':
                    #bpy.ops.object.select_all(action='DESELECT')
                    linked_collection_object.select_set(state=True)
                    bpy.ops.object.duplicates_make_real(use_base_parent=True, use_hierarchy=False)
                    collection_empties.append(linked_collection_object)
                    # Gather all ripped children.
                    children = self.CollectChildren(linked_collection_object)
                    for child in children:
                        if child.type == 'EMPTY' and linked_collection_object.instance_type == 'COLLECTION':
                            # We have nested collection. Proceed further.
                            nested_children, empties = self.CollapseCollection(child)
                            children.extend(nested_children)
                            collection_empties.extend(empties)
                        elif child.type == 'MESH':
                            child.data = child.data.copy()

                            # for mod in child.modifiers:
                            #     bpy.context.view_layer.objects.active = child
                            #     bpy.ops.object.modifier_apply(modifier = mod.name)
                            #bpy.ops.object.modifier_apply(modifier = mod.name)

                    return children, collection_empties

        return objects

    def CollectChildren(self, object):
        children = []
        children.extend(object.children)

        for obj in object.children:
            nested_children = self.CollectChildren(obj)
            children.extend(nested_children)
        return children

    def CollapseObject(self, collapse_object, merge_master = None):
        bpy.ops.object.select_all(action='DESELECT')
        master_collection = bpy.context.scene.collection

        children = []
        trash = []
        merged_mesh_object = None

        if collapse_object.type == 'EMPTY':
            if collapse_object.instance_type == 'COLLECTION':
                collection, empties = self.CollapseCollection(collapse_object)
                trash.extend(empties)

        # Remove?
        elif collapse_object.display_type == 'TEXTURED' or collapse_object.display_type == 'SOLID':
            # Apply modifiers?
            return collapse_object
            merged_mesh_object = collapse_object

        children = self.CollectChildren(collapse_object)

        # Ripping collection from pool of children.
        for child in children:
            if child.type == 'EMPTY' and child.instance_type == 'COLLECTION':
                nested_children, empties = self.CollapseCollection(child)
                trash.extend(empties)
                children.extend(nested_children)

        # Here we assuming that our children collection have no linked objects, so probably
        # meshes, empties, etc.
        #bpy.ops.object.select_all(action='DESELECT')
        for child in children:
            if child.type == 'MESH':
                if child.display_type == 'TEXTURED' or child.display_type == 'SOLID':
                    #bpy.ops.object.select_all(action='DESELECT')
                    child.select_set(state=True)
                    bpy.context.view_layer.objects.active = child
                    bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')
                    for mod in child.modifiers:
                         child.data = child.data.copy()
                         bpy.ops.object.modifier_apply(modifier = mod.name)

                else:
                    trash.append(child)
            else:
                trash.append(child)

        if collapse_object.type != 'MESH':
            trash.append(collapse_object)
            collapse_object.select_set(state=False)
        elif collapse_object.display_type == 'TEXTURED' or collapse_object.display_type == 'SOLID':
            collapse_object.select_set(state=False)
        #if merged_mesh_object:
        bpy.ops.object.join()
        restore_object_selection = bpy.context.view_layer.objects.active

        # Cleanup.

        bpy.ops.object.select_all(action='DESELECT')
        for t in trash:
            t.select_set(state=True)
            bpy.context.view_layer.objects.active = t
        bpy.ops.object.delete()

        del trash
        del children


        return restore_object_selection

    def execute(self, context):

        collapsed_objects = []

        for obj in bpy.context.selected_objects:
            collapsed_objects.append(self.CollapseObject(obj))

        for obj in collapsed_objects:
            obj.select_set(state=True)
            bpy.context.view_layer.objects.active = obj

        return {'FINISHED'}

class BATA_OT_SetupMetrics(bpy.types.Operator):
    bl_idname = "baya.setup_metrics"
    bl_label = "Setup Metrics"
    bl_options = set()

    def execute(self, context):
        print("Setup metrics.")

        bpy.context.scene.unit_settings.system = 'IMPERIAL'
        bpy.context.scene.unit_settings.length_unit = 'INCHES'
        bpy.context.scene.unit_settings.scale_length = 0.001

        return {'FINISHED'}

class OpenSourceFolder(bpy.types.Operator):
    bl_idname = "baya.open_source_folder"
    bl_label = "Open Source Folder"
    bl_options = set()

    def execute(self, context):
        FILEBROWSER_PATH = os.path.join(os.getenv('WINDIR'), 'explorer.exe')
        path = os.path.normpath(GetSourcePath())

        print("Opening source folder: " + path)

        if not os.path.exists(path):
            os.makedirs(path)
        if os.path.isdir(path):
            subprocess.run([FILEBROWSER_PATH, path])
        elif os.path.isfile(path):
            subprocess.run([FILEBROWSER_PATH, '/select,', os.path.normpath(path)])
        return {'FINISHED'}

class ApplyModifiers(bpy.types.Operator):
    bl_idname = "baya.apply_modifiers"
    bl_label = "Apply Visible Modifiers"
    bl_options = set()

    def execute(self, context):
        print("ApplyModifiers")

def register():
    for cls in (GeneratorOptions, BATA_OT_SetupMetrics, BATA_OT_CollapseObject, Maya_Generator_Panel, GenerateMayaFile, OpenSourceFolder, OpenExportFolder, ApplyModifiers):
        bpy.utils.register_class(cls)


    bpy.types.Scene.baya_options = bpy.props.PointerProperty(type=GeneratorOptions)

def unregister():
    for cls in (BATA_OT_SetupMetrics, BATA_OT_CollapseObject, Maya_Generator_Panel, GenerateMayaFile, OpenSourceFolder, OpenExportFolder, GeneratorOptions, ApplyModifiers):
        bpy.utils.unregister_class(cls)
    #bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.baya_options
