# Blender COD Tools. Developed by Peter Gubin.

import bpy
from bpy.props import IntProperty

#from ...utils import *
#from ...utils.data_types import XModel

import sys

if 'DEBUG_MODE' in sys.argv:
    from utils import data_types as dt
    from utils import generic_functions as g
else:
    from ...utils import data_types as dt
    from ...utils import generic_functions as g

from bpy.props import EnumProperty
from bpy.props import BoolProperty
from bpy.props import PointerProperty

class HavokOptions(bpy.types.PropertyGroup):

    def shapetype_action_changed(self, context):
        print(self, self.shape_type, context)

    def surfacetype_action_changed(self, context):
        print(self, self.surface_type, context)

    shape_type = EnumProperty(
    name="Shape Type",
    items=(
        ('Hull', 'Hull', 'Hull represents complex shape'),
        ('Box', 'Box', '    Box represents simple box shape, do not rotate it.'),
        ('Sphere', 'Sphere', ''),
        ('Cylinder', 'Cylinder', ''),
        ('Capsule', 'Capsule', ''),
        ('Mesh', 'Mesh', '')
    ),
    default="Hull",
    update=shapetype_action_changed)

    # Options.
    test = BoolProperty(name='Export only Asset data', default=True)

    @classmethod
    def init(self, context):
        pass



class ExportSettings(bpy.types.PropertyGroup):
    xmodels = bpy.props.CollectionProperty(type=dt.XModel, name="XModels")
    active_xmodel_index = bpy.props.IntProperty(name="Active XModel", default=-1)

    @classmethod
    def get_current_xmodel(self, context):
        settings = context.scene.export_settings
        return settings.xmodels[settings.active_xmodel_index]

    @classmethod
    def init(self, context):
        print("INIT EXPORT SETTINGS")



class COD_UL_XModel_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        settings = context.scene.export_settings
        xmodel = settings.xmodels[index]
        lods_number = len(xmodel.lods)

        split = layout.split(factor=0.55, align=False)

        # name = "XModel:" + xmodel.name +  " [" + str(lods_number) + " LOD"

        polycount = xmodel.calculate_overall_polycount(context, index)

        name = "[" + str(lods_number) + " LOD"

        if lods_number > 1:
            name += "s"

        name += ", polys: "
        name += str(polycount)

        name += "]"

        col = split.column()
        split.label(text=name)
        col.prop(item, "name", text="")
        # col.operator("cod.xmodel_move_up",text="", icon='SORT_DESC').index = index
        layout.operator("cod.xmodel_move_up", text="", icon='SORT_DESC').index = index
        layout.operator("cod.xmodel_move_down", text="", icon='SORT_ASC').index = index
        #layout.operator("cod.xmodel_export", text="", icon='FORWARD').index = index
        layout.operator("cod.xmodel_remove", text="", icon='X').index = index
        # col.operator("cod.xmodel_add", text = "Add New")


class COD_UL_LOD_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        settings = context.scene.export_settings
        xmodel = settings.xmodels[settings.active_xmodel_index]

        polycount = xmodel.calculate_lod_polycount(context, settings.active_xmodel_index, index)
        label = "LOD" + str(item.index) + "  " + str(polycount)
        # label = "LOD" + str(item.index) + ", object: " + item.mesh_name#+ str(len(item.mesh_names))

        layout.label(text=label)

        #column = layout.column()
        #if len(item.collections) <= 0:
        layout.operator("cod.xmodel_lod_collection_set", text="Set", icon='IMPORT').lod_index = index
        #layout.operator("cod.xmodel_lod_collection_add", text="Add Collection").lod_index = index
        #else:
        #else:
            #column.operator("cod.xmodel_lod_content_select", text="SELECT").index = index
            #column.operator("cod.xmodel_lod_content_remove_all", text="REMOVE")

        layout.prop(item, "distance", text="")

        layout.prop(item, "name", text="")
        #layout.operator("cod.xmodel_lod_set", text="", icon='IMPORT').lod_index = index
        #layout.operator("cod.xmodel_lod_export_selected", text="", icon='FORWARD')
        row = layout.row()
        row.operator("cod.xmodel_lod_remove", text="", icon='X').index = index
        if index == 0:
            row.enabled = False


class COD_UL_LOD_Content_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        #print(str(index))
        layout.label(text=item.obj_pointer.name)
        layout.label(text="Test")
        mesh = item.obj_pointer.data
        layout.label(text="tris: " + str(len(mesh.polygons)))
        layout.label(text="verts: " + str(len(mesh.vertices)))
        layout.operator("cod.xmodel_lod_content_select").index = index
        layout.operator("cod.xmodel_lod_content_remove").index = index



class COD_UL_LOD_Collection_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):

        layout.label(text=item.name)

        col = g.FindCollection(item.name)
        if col:
            objects = g.GetNestedObjectsFromCollection(col, 0)
            #objects = col.objects
            #layout.label(text="COUNT = {c}".format(c=str(len(objects))))
            column = layout.column()
            for obj in objects:
                column.label(text=obj.name)
                #column.operator("cod.xmodel_lod_content_select").index = index
        layout.operator("cod.xmodel_lod_collection_select").index = index
        layout.operator("cod.xmodel_lod_collection_remove").index = index

class COD_UL_Havok_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):

        #layout.label(text=item.name)
        col = g.FindCollection(item.name)
        if col:
            objects = g.GetNestedObjectsFromCollection(col, 0)

            column = layout.column()


            for idx, obj in enumerate(objects):
                row = column.row()
                #column = row.column()


                #split = layout.split(factor=0.55, align=False)
                #col = row.column()

                #column.label(text=obj.name)
                row.label(text=obj.name)

                #column = row.column()
                #column.label("asda")
                #column.operator("cod.xmodel_lod_collection_select").index = index
                #reference_object['some_property'] = 5

                #if obj['some_property']:
                    #print("sas")
                row.prop(obj.havok_options, "shape_type", text="")
                #row.prop(obj.havok_options, "surface_type", text="")






class ExportWarning:
    title = "Reason"
    description = "Description"


class XModelNameChangeListener(bpy.types.Operator):
    """Move an object with the mouse, example"""
    bl_idname = "cod.xmodel_name_change_listener"
    bl_label = ""

    def modal(self, context, event):
        print("modal")
        if event.type in {'TEXTINPUT', 'ESC', 'A', 'B'}:
            print("TEXTINPUT")

        elif event.type in {'RIGHTMOUSE', 'ESC'}:
            print("modal cancel")
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}





class COD_Export_Panel(bpy.types.Panel):
    bl_label = "COD XModel Export"
    bl_idname = "cod.xmodel_export"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'COD Tools'
    #bl_space_type = 'PROPERTIES'
    #bl_region_type = 'WINDOW'
    #bl_context = "output"

    def sync_lod_indexes(self, context):
        settings = context.scene.export_settings
        for idx, lod in enumerate(settings.xmodels[settings.active_xmodel_index].lods):
            lod.index = idx

    def show_warning(self, layout, warning):
        box = layout.box()
        row = box.row()
        row.label(text=warning.description)

    @staticmethod
    def check_are_lods_descending(context):
        is_descending = True

        settings = context.scene.export_settings
        xmodel = settings.xmodels[settings.active_xmodel_index]

        for idx, lod in enumerate(xmodel.lods):
            if len(lod.content) == 0:
                warning_polycount_zero = Warning()
                warning_polycount_zero.description = "LOD" + str(idx) + " is empty."
                return warning_polycount_zero
            if idx > 0:
                polycount_current = xmodel.calculate_lod_polycount(context, settings.active_xmodel_index, idx)
                polycount_last = xmodel.calculate_lod_polycount(context, settings.active_xmodel_index, idx - 1)

                # @Todo: Could be more sophisticated.
                if not polycount_current < polycount_last:
                    is_descending = False

        if not is_descending:
            warning = Warning()
            warning.description = "LODs descending hierarchy is wrong."
            return warning
        return None

    def check_too_many_lods(self, xmodel):
        if len(xmodel.lods) > 5:
            warning = Warning()
            warning.description = "Too many lods, consider lowering the amount to less than 5."
            return warning
        return None

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        row = layout.row()
        # row.operator("cod.create_window")
        #row.operator("cod.xmodel_export_all")

        if not scene.auto_convert_launch:
            row = layout.row()
            row.operator("cod.xmodel_converter_launch")

        box = layout.box()

        if scene.export_options:
            box.prop(scene, "export_options", icon="TRIA_DOWN", emboss=False)
            row = box.row()
            #row.prop(scene, "auto_convert_launch")
            #row = box.row()
            row.prop(scene, "auto_sync_names")
            row = box.row()
            row.prop(scene, "auto_correct_name")
            #row = box.row()
            #row.prop(scene, "content_folder")
            # row = box.row()
            # row.prop(scene, "auto_capitalize_name")
            # row = box.row()
            # row.prop(scene, "lod_capitalize")
        else:
            box.prop(scene, "export_options", icon="TRIA_RIGHT", emboss=False)

        row = layout.row()
        row.label(text="XModels", icon="CUBE")

        settings = context.scene.export_settings

        layout.template_list(
            "COD_UL_XModel_List",
            "my_color_list",
            settings,
            "xmodels",
            settings,
            "active_xmodel_index",
            type='DEFAULT',
            rows=4
        )

        row = layout.row(align=True)
        row.operator("cod.xmodel_add")
        #row.operator("cod.xmodel_export_all")


        # Does it even make sense to show data if there aren't
        # any xmodels created?

        #if not settings.active_xmodel_index >= 0 or len(settings.xmodels) == 0:
            #settings.active_xmodel_index = 0
        #if settings.active_xmodel_index < 0:
            #settings.active_xmodel_index = 0
        if settings.active_xmodel_index >= 0 and len(settings.xmodels) > 0:
            xmodel = settings.xmodels[settings.active_xmodel_index]

        #if scene.auto_sync_names:
            #bpy.ops.cod.xmodel_lod_sync_all('INVOKE_DEFAULT')

            item = settings.xmodels[settings.active_xmodel_index]
            if not xmodel.active_lod_index >= 0:
                return

            xmodel_info_box = layout.box()

            #xmodel_info_name = "XModel Information"
            xmodel_info_name = "XModel Info: {name}".format(name = xmodel.name)
            lods_info_name = "LODs Info ({count})".format(count = len(xmodel.lods))
            havok_info_name = "Havok Entities Info"

            if scene.show_xmodel_info:
                xmodel_info_box.prop(scene, "show_xmodel_info", icon="TRIA_DOWN", text = xmodel_info_name, emboss=False)

                #xmodel_info_box.label(text="LODs", icon="PRESET")

                # Warning panel

                warnings = []

                warning = self.check_are_lods_descending(context)
                if warning:
                    warnings.append(warning)
                warning = self.check_too_many_lods(xmodel)
                if warning:
                    warnings.append(warning)

                if len(warnings) > 0:
                    box = xmodel_info_box.box()
                    row = box.row()
                    # row.operator("cod.create_window")
                    # warning_row = box.row()
                    row.label(text="Warnings", icon='ERROR')
                    # row = box.row()

                    for warn in warnings:
                        self.show_warning(box, warn)


                row = xmodel_info_box.column()
                box = xmodel_info_box.box()

                # LODs section.

                if scene.show_lod_info:
                    box.prop(scene, "show_lod_info", icon="TRIA_DOWN", text= lods_info_name, emboss=False)

                    lod = xmodel.lods[xmodel.active_lod_index]
                    self.sync_lod_indexes(context)

                    box.template_list(
                        "COD_UL_LOD_List",
                        "lod_list",
                        item,
                        "lods",
                        item,
                        "active_lod_index",
                        type='DEFAULT',
                        rows=0
                    )

                    row = box.row(align=True)
                    row.operator("cod.xmodel_lod_add")

                    column = row.column(align=True)

                    column.operator("cod.xmodel_lod_remove_all")
                    if not xmodel.does_have_lods(context):
                        column.enabled = False

                    if not scene.auto_sync_names:
                        row = box.row(align=True)
                        row.operator("cod.xmodel_lod_sync")


                    row = box.row()
                    row.label(text="Name")
                    row.label(text=lod.name)

                    row = box.row()
                    row.label(text="Overall triangle count")
                    tri_count = 0
                    vert_count = 0
                    for lod_content_item in lod.content:
                        if lod_content_item.obj_pointer:
                            mesh = lod_content_item.obj_pointer.data
                            tri_count += len(mesh.polygons)
                            vert_count += len(mesh.vertices)
                    row.label(text=str(tri_count))
                    row = box.row()
                    row.label(text="Overall vertex count")
                    row.label(text=str(vert_count))

                    box = box.box()
                    if scene.show_lod_contents:
                        box.prop(scene, "show_lod_contents", icon="TRIA_DOWN", text="LOD Contents", emboss=False)
                        box.template_list(
                            "COD_UL_LOD_Collection_List",
                            "lod_collection_list",
                            lod,
                            "collections",
                            lod,
                            "active_lod_content_index",
                            type='DEFAULT',
                            rows=3
                        )

                        row = box.row(align=True)
                        #row.operator("cod.xmodel_lod_content_include")
                        row.operator("cod.xmodel_lod_content_select_all")
                        row.operator("cod.xmodel_lod_content_remove_all")
                    else:
                        box.prop(scene, "show_lod_contents", icon="TRIA_RIGHT", text="LOD Contents", emboss=False)


                else:
                    box.prop(scene, "show_lod_info", icon="TRIA_RIGHT", text = lods_info_name, emboss=False)

                # Havok section.
                box = xmodel_info_box.box()
                if scene.show_havok_info:
                    box.prop(scene, "show_havok_info", icon="TRIA_DOWN", text= havok_info_name, emboss=False)

                    row = box.row()
                    #row.operator("cod.havok_convert")
                    row.operator("cod.havok_assign_collection")

                    row = box.row()
                    row.operator("cod.havok_toggle_wiremode_on")
                    row.operator("cod.havok_toggle_wiremode_off")


                    havok_content = xmodel.havok

                    row = box.row()
                    row.template_list(
                    "COD_UL_Havok_List",
                    "havok_collection_list",
                    havok_content,
                    "havok_collections",
                    havok_content,
                    "active_havok_collection_index",
                    type='DEFAULT',
                    rows=3
                    )

                    row = box.row()
                    row.operator("cod.havok_remove_entity", text="Unassign Collection", icon='X')

                else:
                    box.prop(scene, "show_havok_info", icon="TRIA_RIGHT", text= havok_info_name, emboss=False)


            else:
                xmodel_info_box.prop(scene, "show_xmodel_info", icon="TRIA_RIGHT", text = xmodel_info_name, emboss=False)




class CreateWindow(bpy.types.Operator):
    bl_idname = "cod.create_window"
    bl_label = "Create Window"
    bl_options = set()

    def execute(self, context):
        # Call user prefs window
        bpy.ops.screen.userpref_show('INVOKE_DEFAULT')

        # Change area type
        area = bpy.context.window_manager.windows[-1].screen.areas[0]
        area.type = 'PROPERTIES'
        return {'FINISHED'}


class MoveUp(bpy.types.Operator):
    bl_idname = "cod.xmodel_move_up"
    bl_label = ""
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.xmodels.move(self.index, self.index - 1)

        return {'FINISHED'}


class MoveDown(bpy.types.Operator):
    bl_idname = "cod.xmodel_move_down"
    bl_label = ""
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.xmodels.move(self.index, self.index + 1)
        return {'FINISHED'}


class AddXModel(bpy.types.Operator):
    bl_idname = "cod.xmodel_add"
    bl_label = "Add XModel"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.xmodels.add()
        xmodel.init(context)
        settings.active_xmodel_index = len(settings.xmodels) - 1

        settings.get_current_xmodel(context).add_lod(context)
        return {'FINISHED'}


class RemoveXModel(bpy.types.Operator):
    bl_idname = "cod.xmodel_remove"
    bl_label = "Remove Active XModel"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        return context.scene.export_settings.active_xmodel_index >= 0

    def execute(self, context):
        settings = context.scene.export_settings
        settings.xmodels.remove(self.index)
        settings.active_xmodel_index -= 1
        if settings.active_xmodel_index < 0 or settings.active_xmodel_index > len(settings.xmodels):
            settings.active_xmodel_index = len(settings.xmodels)
        return {'FINISHED'}


class ExportXmodel(bpy.types.Operator):
    bl_idname = "cod.xmodel_export"
    bl_label = "Export"
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.xmodels[self.index]

        print("Export xmodel with index: " + str(self.index))

        if context.scene.auto_convert_launch:
            print("Auto launch converter: true.")
            bpy.ops.cod.xmodel_converter_launch('INVOKE_DEFAULT')
        return {'FINISHED'}


class ExportAllXModels(bpy.types.Operator):
    bl_idname = "cod.xmodel_export_all"
    bl_label = "Export All"
    bl_options = set()

    def execute(self, context):
        print("All XModels have been exported.")
        if context.scene.auto_convert_launch:
            print("Auto launch converter: true.")
            bpy.ops.cod.xmodel_converter_launch('INVOKE_DEFAULT')

        return {'FINISHED'}


def run_batch_file(file_path):
    import subprocess
    subprocess.Popen(file_path, creationflags=subprocess.CREATE_NEW_CONSOLE)


class LaunchConverter(bpy.types.Operator):
    bl_idname = "cod.xmodel_converter_launch"
    bl_label = "Launch converter"
    bl_options = set()

    def execute(self, context):
        run_batch_file(context.scene.content_folder + "\\converter.bat")
        print("Converter invoked.")
        return {'FINISHED'}


# GET MODEL BY INDEX OR GET SELECTED MODEL METHOD
# GET LOD BY INDEX OR GET SELECTED LOD METHOD

class ExportAllLODs(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_export_all"
    bl_label = "Export All LODs"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.xmodels[settings.active_xmodel_index]
        lods = xmodel.lods

        for lod in lods:
            print("LOD :: " + lod.name + ":: exported.")
        return {'FINISHED'}


class ExportSelectedLOD(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_export_selected"
    bl_label = "Export"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.xmodels[settings.active_xmodel_index]
        lod = xmodel.lods[xmodel.active_lod_index]
        print("LOD :: " + lod.name + ":: exported.")
        return {'FINISHED'}


class AddLOD(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_add"
    bl_label = "Add LOD"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        settings.xmodels[settings.active_xmodel_index].add_lod(context)
        return {'FINISHED'}


class RemoveLOD(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_remove"
    bl_label = "Remove"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        settings = context.scene.export_settings
        xmodel = settings.xmodels[settings.active_xmodel_index]
        return xmodel.active_lod_index >= 0

    def execute(self, context):
        settings = context.scene.export_settings
        settings.xmodels[settings.active_xmodel_index].remove_lod(context, self.index)
        return {'FINISHED'}


class RemoveAllLODs(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_remove_all"
    bl_label = "Clear All"

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.xmodels[settings.active_xmodel_index]
        xmodel.clear_lods(context)
        return {'FINISHED'}


class SetLOD(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_collection_set"
    bl_label = "Set"
    bl_options = set()
    lod_index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        lod = xmodel.lods[self.lod_index]
        print("Setting LOD information for: " + lod.name + ".")

        # Setting LOD content.
        lod.collections.clear()
        lod.content.clear()

        #Check selected collection in outliner.
        active_collection = bpy.context.view_layer.active_layer_collection
        collection = lod.collections.add()
        collection.name = active_collection.name

        #print("Added new collection to {lod}: {collection}".format(lod=lod.name, collection=collection.name))
        return {'FINISHED'}

class AddCollection(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_collection_add"
    bl_label = "Add"
    bl_options = set()
    lod_index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        lod = xmodel.lods[self.lod_index]
        print("Adding LOD information for: " + lod.name + ".")

        #Check selected collection in outliner.
        active_collection = bpy.context.view_layer.active_layer_collection

        if lod.collections.get(active_collection.name) is None:
            collection = lod.collections.add()
            collection.name = active_collection.name

            print("Added new collection to {lod}: {collection}".format(lod=lod.name, collection=collection.name))
        return {'FINISHED'}


        for obj in bpy.context.selected_objects:
            obj_type = getattr(obj, 'type', '')
            if obj_type == 'MESH':
                content = lod.content.add()
                content.name = obj.name
                content.obj_pointer = bpy.data.objects.get(obj.name)
        return {'FINISHED'}

class SyncLODNamesAll(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_sync_all"
    bl_label = "Sync LOD Names"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings

        for xmodel in settings.xmodels:
            index = 0
            for lod in xmodel.lods:
                suggested_name = settings.get_current_xmodel(context).name + "_lod" + str(index)
                lod.name = suggested_name
                index += 1
        return {'FINISHED'}


class SyncLODNames(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_sync"
    bl_label = "Sync LOD Names"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings

        index = 0
        for lod in settings.get_current_xmodel(context).lods:
            suggested_name = settings.get_current_xmodel(context).name + "_lod" + str(index)
            lod.name = suggested_name
            index += 1
        return {'FINISHED'}



class SelectContent(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_content_select"
    bl_label = "Select"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        lod = xmodel.lods[xmodel.active_lod_index]

        lod.active_lod_content_index = self.index
        bpy.ops.object.select_all(action='DESELECT')
        content_to_select = lod.content[self.index].obj_pointer
        bpy.context.view_layer.objects.active = content_to_select
        content_to_select.select_set(state=True)
        return {'FINISHED'}

class SelectCollection(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_collection_select"
    bl_label = "Select"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        lod = xmodel.lods[xmodel.active_lod_index]

        layer_collection = bpy.context.view_layer.layer_collection
        col = g.RecursiveSearchLayerCollection(layer_collection, 'ee_spomenik_monument_02_LOD1')
        bpy.context.view_layer.active_layer_collection = col
        return {'FINISHED'}

class SelectAllContent(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_content_select_all"
    bl_label = "Select All"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        lod = xmodel.lods[xmodel.active_lod_index]
        bpy.ops.object.select_all(action='DESELECT')

        for content in lod.content:
            content_to_select = content.obj_pointer
            bpy.context.view_layer.objects.active = content_to_select
            content_to_select.select_set(state=True)
        return {'FINISHED'}


class IncludeContent(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_content_include"
    bl_label = "Include"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        lod = xmodel.lods[xmodel.active_lod_index]

        return {'FINISHED'}

        for obj in bpy.context.selected_objects:
            obj_type = getattr(obj, 'type', '')
            if obj_type == 'MESH':
                bAlreadyIncluded = False
                for item in lod.content:
                    content_item = item.obj_pointer
                    if content_item.data == obj.data:
                        bAlreadyIncluded = True

                if not bAlreadyIncluded:
                    content = lod.content.add()
                    content.name = obj.name
                    content.obj_pointer = bpy.data.objects.get(obj.name)
        return {'FINISHED'}


class RemoveContent(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_content_remove"
    bl_label = "Remove"
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        lod = xmodel.lods[xmodel.active_lod_index]
        lod.remove_content_by_index(context, self.index)
        print("Remove selected LOD Content with index: " + str(self.index))
        return {'FINISHED'}

class RemoveCollection(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_collection_remove"
    bl_label = "Remove"
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        lod = xmodel.lods[xmodel.active_lod_index]
        lod.collections.remove(self.index)
        return {'FINISHED'}



class RemoveAllContent(bpy.types.Operator):
    bl_idname = "cod.xmodel_lod_content_remove_all"
    bl_label = "Clear Content"
    bl_options = set()

    def execute(self, context):
        print("Remove all LOD Content")
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        lod = xmodel.lods[xmodel.active_lod_index]

        lod.collections.clear()
        return {'FINISHED'}


class Havok_AssignCollection(bpy.types.Operator):
    bl_idname = "cod.havok_assign_collection"
    bl_label = "Assign Collection"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        havok = xmodel.havok

        # Setting LOD content.
        #lod.collections.clear()
        #lod.content.clear()

        #Check selected collection in outliner.
        active_collection = bpy.context.view_layer.active_layer_collection
        collection = havok.havok_collections.add()
        collection.name = active_collection.name

        #print("Added new collection to {lod}: {collection}".format(lod=lod.name, collection=collection.name))
        return {'FINISHED'}


class Havok_Convert(bpy.types.Operator):
    bl_idname = "cod.havok_convert"
    bl_label = "Convert to Havok Entity"
    bl_options = set()

    def execute(self, context):
        print("Convert to Havok Entity.")
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        #lod = xmodel.lods[xmodel.active_lod_index]

        obj = bpy.context.view_layer.objects.active
        obj.is_havok_shape = True
        #obj.havok_options.shape_type = Box
        #obj['some_property'] = 5
        #obj['some_property'] = 5

        return {'FINISHED'}


class Havok_RemoveEntity(bpy.types.Operator):
    bl_idname = "cod.havok_remove_entity"
    bl_label = ""
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        return xmodel.havok.havok_collections

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)

        xmodel.havok.havok_collections.clear()

        print("Remove selected LOD Content with index: " + str(self.index))
        return {'FINISHED'}

class Havok_ToggleWireMode_On(bpy.types.Operator):
    bl_idname = "cod.havok_toggle_wiremode_on"
    bl_label = "Wire On"
    bl_options = set()

    @classmethod
    def poll(cls, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        return xmodel.havok.havok_collections

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        havok = xmodel.havok
        if len(havok.havok_collections) > 0:
            collection = havok.havok_collections[0]
            col = g.FindCollection(collection.name)
            if col:
                objects = g.GetNestedObjectsFromCollection(col, 0)
                for obj in objects:
                        obj.display_type = 'WIRE'
        return {'FINISHED'}

class Havok_ToggleWireMode_Off(bpy.types.Operator):
    bl_idname = "cod.havok_toggle_wiremode_off"
    bl_label = "Solid On"
    bl_options = set()

    @classmethod
    def poll(cls, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        return xmodel.havok.havok_collections

    def execute(self, context):
        settings = context.scene.export_settings
        xmodel = settings.get_current_xmodel(context)
        havok = xmodel.havok
        if len(havok.havok_collections) > 0:
            collection = havok.havok_collections[0]
            col = g.FindCollection(collection.name)
            if col:
                objects = g.GetNestedObjectsFromCollection(col, 0)
                for obj in objects:
                        obj.display_type = 'SOLID'
        return {'FINISHED'}

# class RenameUVSets(bpy.types.Operator):
#     bl_idname = "cod.xmodel_rename_uv_sets"
#     bl_label = "Rename UV Sets"
#     bl_options = set()
#
#     def execute(self, context):
#         print("Remove all LOD Content")
#         return {'FINISHED'}


def register():
    for cls in (XModelNameChangeListener, CreateWindow, COD_UL_XModel_List, COD_UL_LOD_List,
                COD_UL_LOD_Content_List, COD_UL_LOD_Collection_List, COD_UL_Havok_List, COD_Export_Panel, SetLOD, AddLOD, RemoveLOD, RemoveAllLODs, ExportXmodel,
                ExportAllXModels, AddCollection,
                SyncLODNames, SyncLODNamesAll, AddXModel, RemoveXModel, ExportSelectedLOD, ExportAllLODs,
                SelectAllContent, SelectContent, SelectCollection, RemoveCollection,
                HavokOptions, Havok_Convert, Havok_AssignCollection, Havok_RemoveEntity, Havok_ToggleWireMode_On, Havok_ToggleWireMode_Off,
                IncludeContent, RemoveContent, RemoveAllContent, MoveUp, MoveDown, LaunchConverter):
        bpy.utils.register_class(cls)

    #bpy.types.Scene.lod_curve = bpy.props.PointerProperty(type=ShaderNodeRGBCurve)

    bpy.types.Scene.content_folder = bpy.props.StringProperty(name='Content folder', default="C:/trees")
    bpy.types.Scene.show_xmodel_info = bpy.props.BoolProperty(name='Show XModel info', default=False)
    bpy.types.Scene.show_lod_info = bpy.props.BoolProperty(name='Show LOD info', default=False)
    bpy.types.Scene.show_havok_info = bpy.props.BoolProperty(name='Havok info', default=False)
    bpy.types.Scene.show_lod_contents = bpy.props.BoolProperty(name='Show LOD Contents', default=False)


    bpy.types.Scene.export_options = bpy.props.BoolProperty(name='Options', default=False)
    bpy.types.Scene.auto_convert_launch = bpy.props.BoolProperty(name='Automatic converter launch', default=True,
                                                                 description="Automaticly laucnhes converter after each export.")
    bpy.types.Scene.auto_sync_names = bpy.props.BoolProperty(name='Auto Sync LOD Names',
                                                             description="Automaticly rename LODs according to XModel name",
                                                             default=True)
    # bpy.types.Scene.auto_sync_content_names = bpy.props.BoolProperty(name='Auto Sync Content Names', description="Automaticly rename LOD content object names according to LOD name with adding postfix.",default=True)
    bpy.types.Scene.auto_correct_name = bpy.props.BoolProperty(name='Auto Correct Names', default=True)
    # bpy.types.Scene.auto_capitalize_name = bpy.props.BoolProperty(name='Auto Capitalize Names', default=False)
    # bpy.types.Scene.lod_capitalize = bpy.props.BoolProperty(name='Capitalize LOD Postfix', default=True)

    # bpy.ops.cod.xmodel_name_change_listener('INVOKE_DEFAULT')
    bpy.types.Object.is_havok_shape = bpy.props.BoolProperty(name='Is Havok Shape', default=False)
    bpy.types.Object.havok_options = bpy.props.PointerProperty(type=HavokOptions)

def unregister():
    for cls in (XModelNameChangeListener, CreateWindow, COD_UL_XModel_List, COD_UL_LOD_List,
                COD_UL_LOD_Content_List, COD_UL_LOD_Collection_List, COD_UL_Havok_List, COD_Export_Panel, SetLOD, AddLOD, RemoveLOD, RemoveAllLODs, ExportXmodel,
                ExportAllXModels, AddCollection,
                SyncLODNames, SyncLODNamesAll, AddXModel, RemoveXModel, ExportSelectedLOD, ExportAllLODs,
                SelectAllContent, SelectContent, SelectCollection, RemoveCollection,
                HavokOptions, Havok_Convert, Havok_AssignCollection, Havok_RemoveEntity, Havok_ToggleWireMode_On, Havok_ToggleWireMode_Off,
                IncludeContent, RemoveContent, RemoveAllContent, MoveUp, MoveDown, LaunchConverter):
        bpy.utils.unregister_class(cls)

    del bpy.types.Scene.content_folder
    del bpy.types.Scene.show_lod_info
    del bpy.types.Scene.show_lod_contents
    del bpy.types.Scene.export_options
    del bpy.types.Scene.auto_convert_launch
    del bpy.types.Scene.auto_sync_names
    del bpy.types.Scene.auto_correct_name
    del bpy.types.Object.is_havok_shape
    del bpy.types.Object.havok_options

if __name__ == "__main__":
    register()
