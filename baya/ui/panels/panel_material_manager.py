# Developed by Peter Gubin.
# Twitter: @GubinPetr
# petr.gubin@gmail.com

import bpy
import sys

if 'DEBUG_MODE' in sys.argv:
    from utils import *
    from utils import data_types as dt
    from utils import generic_functions as g
else:
    from ...utils import *
    from ...utils import data_types as dt
    from ...utils import generic_functions as g

class MaterialManagerSettings(bpy.types.PropertyGroup):
    active_material_index = bpy.props.IntProperty(name="Active Material", default=-1)

    section_material_reassign = bpy.props.BoolProperty(name='Material reassign', default=False)
    section_material_load = bpy.props.BoolProperty(name='Material load', default=False)
    section_material_list = bpy.props.BoolProperty(name='Material list', default=True)

class Baya_UL_Material_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):

        split1 = layout.split(factor=0.6)
        split2 = layout.split(factor=0.2)

        col1, col3 = (split1.row(),split1.row())
        col1.prop(item, "name", text="U: {users_count}".format(users_count = item.users))

        col3.operator("baya.material_assign", text="Assign").index = index
        col3.operator("baya.material_delete", text="", icon='X').index = index


class Baya_Material_Manager_Panel(bpy.types.Panel):
    bl_label = "Baya Material Manager"
    bl_idname = "baya.material_manager"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Baya'

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        settings = context.scene.material_manager_settings

        row = layout.row()
        row.label(text="Material Manager")

        # Material reassign tool.
        box = layout.box()
        if settings.section_material_reassign:
            box.prop(settings, "section_material_reassign", icon="TRIA_DOWN", emboss=False)

            row = box.row()
            row.label(text="Material Reassign Tool")

            row = box.row()
            row.prop(scene, "material_reassign_delete")

            row = box.row()
            row.prop_search(scene,
                               "material_reassign_source",
                               bpy.data,
                               "materials", text = "From")
            row.prop_search(scene,
                               "material_reassign_target",
                               bpy.data,
                               "materials", text = "to")

            row = box.row()
            row.operator("baya.material_reassign", text = "Reassign material")
        else:
            box.prop(settings, "section_material_reassign", icon="TRIA_RIGHT", emboss=False)

        # Material list.
        box = layout.box()
        if settings.section_material_list:
            box.prop(settings, "section_material_list", icon="TRIA_DOWN", emboss=False)

            row = box.row()

            # Material cleanup.
            box.template_list("Baya_UL_Material_List",
                                 "",
                                 bpy.data,
                                 "materials",
                                 settings,
                                 "active_material_index",
                                 rows=4)


            count = 0
            for material in bpy.data.materials:
                if not material.users:
                    count += 1
            row = box.row()

            if count == 0:
                row.operator("baya.materials_cleanup", text = "No unused materials")
                row.enabled = False
            else:
                row.operator("baya.materials_cleanup", text = "Purge {count} unused materials".format(count = count))


        else:
            box.prop(settings, "section_material_list", icon="TRIA_RIGHT", emboss=False)





def register():
    for cls in (
            Baya_UL_Material_List, Baya_Material_Manager_Panel, MaterialManagerSettings):
        bpy.utils.register_class(cls)

    bpy.types.Scene.material_manager_settings = bpy.props.PointerProperty(type=MaterialManagerSettings)
    bpy.types.Scene.material_reassign_source = bpy.props.StringProperty(name='', default="")
    bpy.types.Scene.material_reassign_target = bpy.props.StringProperty(name='', default="")
    bpy.types.Scene.material_reassign_delete = bpy.props.BoolProperty(name='Delete after     reassignment', default=False)



def unregister():
    for cls in (
            Baya_UL_Material_List, Baya_Material_Manager_Panel, MaterialManagerSettings):
        bpy.utils.unregister_class(cls)

    del bpy.types.Scene.material_manager_settings
    del bpy.types.Scene.material_reassign_source
    del bpy.types.Scene.material_reassign_target
    del bpy.types.Scene.material_reassign_delete




if __name__ == "__main__":
    register()
